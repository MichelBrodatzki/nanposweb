# NANPOS Web

Webfrontend for [nanpos](https://github.com/antonxy/nanpos)

## Config

create `instance/config.py`. All [Flask](https://flask.palletsprojects.com/en/2.0.x/)
/ [Flask-Login](https://flask-login.readthedocs.io/en/latest/)
/ [Flask-SQLAlchemy](https://flask-sqlalchemy.palletsprojects.com/en/2.x/) config values are possible.

For production at least DB-Connection & Secret-Key are required / recommended:

```python
SECRET_KEY = 'secret-key'
SQLALCHEMY_DATABASE_URI = 'postgresql://nanpos:nanpos@localhost:5432/nanpos'
```

A Secret key can be generated with:

```python
import secrets
secrets.token_urlsafe(16)
```

Other customizable and their default values are:

````python
TERMINAL_LOGOUT_TIMEOUT = 30  # logout timeout for Terminal mode in seconds, set to none to disable
````

## Init

create db-tables:

```python
from nanposweb import create_app
from nanposweb.db import db

app = create_app()
app.app_context().push()
db.create_all()
```

create admin user:

```python
from nanposweb.db import db
from nanposweb.db.models import User
from nanposweb.helpers import calc_hash
from nanposweb import create_app

app = create_app()
app.app_context().push()

admin = User(name='admin', isop=True, pin=calc_hash('1234'))

db.session.add(admin)
db.session.commit()
```

### Bank Data
If you want to display bank account informations, you can define the variable `BANK_DATA` inside the instance config.
Keys and Values will be used inside the table. If `BANK_DATA` is undefined or `None` the page will not be linked in the navigation.
```python
BANK_DATA = {
    'Owner': 'Max Mustermann',
    'IBAN': '123455',
    'BIC': 'ABCDE',
    'Bank': 'Musterbank'
}
```


### Point of Sales
If you want to allow access to the point of sales API, you can enable the API with the variable `ENABLE_POS` and add the IPs of the POS to the `ALLOWED_POS_IPS` list.

```python
ENABLE_POS = True
ALLOWED_POS_IPS = [
    "127.0.0.1",
    "192.168.0.5"
]
```


### OpenID Connect
This project includes a naive implementation of OpenID Connect. To activate it, you have to add your providers in the ```OIDC_PROVIDERS``` list. All OIDC provider objects need the following keys:
- name: Name of the Provider
- color: Color of the button on the login page. The colors are bootstrap colors (e.g. danger, info, warning, ...)
- oidc_configuration: URL to the openid-configuration page
- client_id: The client_id for the oidc provider
- secret: The corresponding secret to the client_id

If you want to automatically promote accounts to admins once they're in the correct group, add the group names into the ```OIDC_ADMIN_GROUPS``` list. Upon login the accounts will be promoted to admin once they're inside the group and demoted to normal user if they ever leave the group.

```python
OIDC_PROVIDERS = [
    {'name': 'OIDC Provider', 'color': 'info', 'oidc_configuration': 'https://example.com/application/o/example/.well-known/openid-configuration', 'client_id': 'EXAMPLE_CLIENT_ID', 'secret': 'VERY_SECRET_CLIENT_SECRET'}
]

OIDC_ADMIN_GROUPS = [
    "Nanposweb Admins"
]
```