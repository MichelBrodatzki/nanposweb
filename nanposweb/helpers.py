from hashlib import sha256
from typing import Optional

from flask import current_app, session
from flask_login import current_user

from .db.models import Revenue, User


def format_currency(value: int, factor: int = 100) -> str:
    return f'{value / factor:.2f} €'.replace('.', ',')


def check_hash(hash_to_check: str, value: str) -> bool:
    hashed_value = sha256(value.encode('utf-8')).hexdigest()

    if hash_to_check == hashed_value:
        return True

    return False


def calc_hash(value: str) -> str:
    return sha256(value.encode('utf-8')).hexdigest()


def get_user_id() -> int:
    impersonate_user_id = session.get('impersonate', None)
    return impersonate_user_id if impersonate_user_id is not None else current_user.id

def is_user_frozen() -> bool:
    impersonate_user_id = session.get('impersonate', None)

    if impersonate_user_id is not None:
        impersonate_user = User.query.filter_by(id=impersonate_user_id).one_or_none()

        if impersonate_user is None:
            return False

        return impersonate_user.frozen

    return current_user.frozen

def revenue_is_cancelable(revenue: Optional[Revenue]) -> bool:
    if revenue is None:
        return False

    if revenue.age.total_seconds() < current_app.config.get('QUICK_CANCEL_SEC', 0):
        return True

    return False
