from typing import Union

from flask import Blueprint, request

from .helpers import allowed_pos_ips_only
from ..db import db
from ..db.helpers import get_balance
from ..db.models import Product, Revenue, User
from ..helpers import calc_hash

pos_bp = Blueprint('pos', __name__, url_prefix='/pos', template_folder='templates')


@pos_bp.route('/', methods=['OPTIONS'])
@allowed_pos_ips_only
def test_pos() -> tuple[list, int]:
    return [], 200

@pos_bp.route('/card', methods=['GET'])
@allowed_pos_ips_only
def get_user_for_card() -> Union[tuple[list, int], tuple[dict, int]]:
    card = request.args.get('id', None)

    if card is None:
        return [], 400

    user = User.query.filter_by(card=calc_hash(card)).one_or_none()

    if user is None:
        return [], 404

    balance = get_balance(user_id=user.id)

    return {
        'user': {
            'id': user.id,
            'name': user.name,
            'balance': balance
        }
    }, 200


@pos_bp.route('/product', methods=['GET'])
@allowed_pos_ips_only
def get_product_by_ean() -> Union[tuple[list, int], tuple[dict, int]]:
    ean = request.args.get('ean', None)

    if ean is None:
        return [], 400

    product = Product.query.filter_by(ean=ean).one_or_none()

    if product is None or not product.visible:
        return [], 404

    return {
        'product': {
            'id': product.id,
            'name': product.name,
            'price': product.price,
            'has_alc': product.has_alc,
            'is_food': product.is_food
        }
    }, 200

@pos_bp.route('/transaction', methods=['POST'])
@allowed_pos_ips_only
def create_transaction() -> Union[tuple[list, int], tuple[dict, int]]:
    product_id = request.form.get('product_id', None)
    user_id = request.form.get('user_id', None)

    if user_id is None or product_id is None:
        return [], 404

    user = User.query.filter_by(id=user_id).one_or_none()
    product = Product.query.filter_by(id=product_id).one_or_none()

    if user is None or product is None:
        return [], 404

    if user.frozen:
        return {'message': 'Konto gesperrt'}, 403

    old_balance = get_balance(int(user_id))

    rev = Revenue(user=user_id, product=product_id, amount=-product.price)
    db.session.add(rev)
    db.session.commit()

    new_balance = get_balance(int(user_id))

    return {
        'product': {
            'id': product.id,
            'name': product.name,
            'price': product.price,
            'has_alc': product.has_alc,
            'is_food': product.is_food
        },
        'user': {
            'name': user.name,
            'balance': {
                'old': old_balance,
                'new': new_balance
            }
        }
    }, 200
