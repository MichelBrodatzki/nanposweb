from functools import wraps
from typing import Any, Callable, Union

from flask import current_app, request


def allowed_pos_ips_only(func): # noqa
    @wraps(func)
    def wrapper(*args: list[Any], **kwargs: dict[Any, Any]) -> Union[tuple[list, int], Callable]:
        allowed_ips = current_app.config.get('ALLOWED_POS_IPS', [])

        remote_ip = request.environ.get('HTTP_X_FORWARDED_FOR', request.environ.get('REMOTE_ADDR', None))

        if remote_ip is None:
            return [], 500

        if remote_ip not in allowed_ips:
            return [], 403

        return func(*args, **kwargs)

    return wrapper
