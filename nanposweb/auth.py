from typing import Union

from flask import Blueprint, current_app, flash, redirect, render_template, request, session, url_for
from flask_login import current_user, login_required, login_user, logout_user
from flask_principal import AnonymousIdentity, Identity, identity_changed
from werkzeug.wrappers import Response

from .db.models import User
from .forms import LoginForm
from .helpers import check_hash

auth_bp = Blueprint('auth', __name__)


@auth_bp.route('/login', methods=['GET', 'POST'])
def login() -> Union[Response, str]:
    if current_user.is_authenticated:
        flash('Already logged in.')
        return redirect(request.args.get('next') or url_for('main.index'))

    session['terminal'] = request.args.get('terminal', False, type=bool)
    form = LoginForm()
    oidc_providers = current_app.config.get('OIDC_PROVIDERS', [])

    if request.method == 'POST':
        if form.validate_on_submit():
            user = User.query.filter_by(name=form.username.data, external=False).one_or_none()

            if user and check_hash(user.pin, form.pin.data or ''):
                login_user(user, remember=form.remember.data)
                flash('Logged in', category='success')

                identity_changed.send(current_app._get_current_object(), identity=Identity(user.id))  # type: ignore

                return redirect(request.args.get('next') or url_for('main.index'))

            flash('Please check your login details and try again.', category='danger')
        else:
            flash('Submitted form was not valid!', category='danger')

    return render_template('login.html', form=form, oidc_providers=oidc_providers)


@auth_bp.route('/logout')
@login_required
def logout() -> Response:
    logout_user()

    # Remove session keys set by Flask-Principal
    for key in ('identity.id', 'identity.auth_type'):
        session.pop(key, None)

    session.pop('impersonate', None)  # remove impersonation status to avoid weird behaviours

    # Tell Flask-Principal the user is anonymous
    identity_changed.send(current_app._get_current_object(), identity=AnonymousIdentity())  # type: ignore
    flash('Logged out')

    if session.get('terminal', False):
        return redirect(url_for('auth.login', terminal=True))

    return redirect(url_for('auth.login'))
