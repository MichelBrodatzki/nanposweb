import json
import secrets
import urllib.parse
from typing import Union

import requests
from flask import Blueprint, current_app, flash, redirect, request, session, url_for
from flask_login import login_user
from flask_principal import Identity, identity_changed
from werkzeug.wrappers import Response

from .helpers import get_provider_configuration, get_provider_with_name
from ..db import db
from ..db.models import User

oidc_bp = Blueprint('oidc', __name__, url_prefix='/oidc', template_folder='templates')

@oidc_bp.route('/<provider_name>', methods=['GET'])
def redirect_to_provider(provider_name: str) -> Union[tuple[list, int], Response]:
    provider = get_provider_with_name(provider_name)

    if provider is None:
        return [], 404

    if 'client_id' not in provider or 'oidc_configuration' not in provider:
        flash('Authentication provider configuration invalid', category='danger')
        return redirect(url_for('auth.login'))

    provider_conf = get_provider_configuration(provider['oidc_configuration'])

    if provider_conf is None:
        flash('Configuration error', category='danger')
        return redirect(url_for('auth.login'))

    auth_endpoint = provider_conf['authorization_endpoint']
    state = secrets.token_urlsafe(32)
    session['oidc_state'] = state

    oidc_params = {
        'response_type': 'code',
        'scope': 'openid profile',
        'state': state,
        'client_id': provider['client_id'],
        'redirect_uri': url_for('oidc.provider_callback', provider_name=provider_name, _external=True)
    }

    return redirect(f'{auth_endpoint}?{urllib.parse.urlencode(oidc_params)}')

@oidc_bp.route('/<provider_name>/callback', methods=['GET'])
def provider_callback(provider_name: str) -> Union[tuple[list, int], Response]:
    provider = get_provider_with_name(provider_name)

    if provider is None:
        return [], 404

    oidc_error = request.args.get('error')
    if oidc_error is not None:
        flash('OpenID Connect request error occurred!', category='danger')
        return redirect(url_for('auth.login'))

    oidc_code = request.args.get('code')
    oidc_state = request.args.get('state')

    if oidc_code is None or oidc_state is None:
        flash('OpenID Connect response error occurred!', category='danger')
        return redirect(url_for('auth.login'))

    if oidc_state != session.get('oidc_state'):
        flash('OpenID Connect state error occurred!', category='danger')
        return redirect(url_for('auth.login'))

    session.pop('oidc_state')

    oidc_configuration = get_provider_configuration(provider['oidc_configuration'])
    if oidc_configuration is None:
        flash('Configuration error', category='danger')
        return redirect(url_for('auth.login'))

    token_request_data = {
        'grant_type': 'authorization_code',
        'code': oidc_code,
        'client_id': provider['client_id'],
        'client_secret': provider['secret'],
        'redirect_uri': url_for('oidc.provider_callback', provider_name=provider_name, _external=True)
    }
    token_r = requests.post(oidc_configuration['token_endpoint'], data=token_request_data, timeout=7)

    if token_r.status_code != 200:
        flash('OpenID Connect token error occurred!', category='danger')
        return redirect(url_for('auth.login'))

    oidc_data = json.loads(token_r.text)
    session['oidc_data'] = oidc_data

    userinfo_headers = {
        'Authorization': f'Bearer {oidc_data['access_token']}'
    }

    userinfo_r = requests.get(oidc_configuration['userinfo_endpoint'], headers=userinfo_headers, timeout=7)

    if userinfo_r.status_code != 200:
        flash('OpenID Connect user info error occurred!', category='danger')
        return redirect(url_for('auth.login'))

    userinfo = json.loads(userinfo_r.text)

    user = User.query.filter_by(name=userinfo['preferred_username'], external=True).one_or_none()

    op_groups = current_app.config.get('OIDC_ADMIN_GROUPS', [])
    is_op = any(group in userinfo['groups'] for group in op_groups)

    if user is None:
        user = User(name=userinfo['preferred_username'], external=True, isop=is_op)
        db.session.add(user)
        db.session.commit()

    if user.isop != is_op:
        user.isop = is_op
        db.session.commit()

    login_user(user, remember=False)

    flash('Logged in', category='success')

    identity_changed.send(current_app._get_current_object(), identity=Identity(user.id))  # type: ignore

    return redirect(request.args.get('next') or url_for('main.index'))
