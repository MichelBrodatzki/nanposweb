import json
from typing import Union

import requests
from flask import current_app


def get_provider_with_name(name: str) -> Union[dict, None]:
    providers = current_app.config.get('OIDC_PROVIDERS', [])

    for provider in providers:
        if 'name' in provider and provider['name'].lower() == name.lower():
                return provider

    return None

def get_provider_configuration(oidc_configuration_url: str) -> Union[dict, None]:
    conf_r = requests.get(oidc_configuration_url, timeout=7)

    if conf_r.status_code == 200:
        return json.loads(conf_r.text)

    return None
